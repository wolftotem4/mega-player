

module.exports = {
  entry: "./index.js",
  output: {
    filename: "mega.js",
    path: __dirname + "/dist",
    library: "MegaPlayer",
    libraryTarget: "umd"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
        }
      }
    ]
  },
  externals: {
    axios: "axios",
    "crypto-js": "CryptoJS"
  }
};