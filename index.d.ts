export interface MegaFileDecryptor
{
    start(pos : number) : MegaFileDecryptor;
    decrypt(data : ArrayBuffer) : Promise<ArrayBuffer>;
}