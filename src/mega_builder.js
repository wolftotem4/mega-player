import axios from 'axios';
import Mega from './mega'
import MegaDecryptStrategy from './mega_decrypt_strategy';
import MegaUtil from './mega_util';

class MegaBuilder
{
    /**
     * @param  {Array<object>}  bases
     * @return {Promise<Array<Mega>>}
     */
    create(bases)
    {
        return this.createBaseInfo(bases)
            .then(this.retrieveMeta.bind(this))
            .then(infoes => {
                return infoes.map(info => new Mega(
                    info.id,
                    info.decryptor.fileDecryptor,
                    info.url,
                    info.filename,
                    info.filesize
                ));
            }
        );
    }

    /**
     * @param {Array<object>} bases 
     */
    createBaseInfo(bases)
    {
        return Promise.all(bases.map(base => {
            let key  = this.parseKey(base.key);
            return MegaDecryptStrategy.create(key.key, key.iv).then(decryptor => {
                return {
                    id: base.id,
                    key: key.key,
                    iv: key.iv,
                    decryptor
                }
            });
        }));
    }

    /**
     * @param  {Array<object>}  infoes
     * @return {Promise<Array<object>>}
     */
    retrieveMeta(infoes)
    {
        let ids = infoes.map(info => info.id);
        return this.fetchMeta(ids).then(data => {
            return Promise.all(data.map((meta, index) => {
                var info = infoes[index];
                return info.decryptor.decryptAttributes(meta.at).then(attributes => {
                    info.url = meta.g;
                    info.filename = attributes.n;
                    info.filesize = meta.s;
                    return info;
                });
            }));
        });
    }

    /**
     * @param  {string}  str
     * @return {object}
     */
    parseKey(str) {
        let a32 = CryptoJS.enc.Base64.parse(MegaUtil.megaToBase64(str)).words;
        let arr = [a32[0]^a32[4],a32[1]^a32[5],a32[2]^a32[6],a32[3]^a32[7],a32[4],a32[5]];

        let key = new Uint8Array(16), keyView = new DataView(key.buffer);
        keyView.setUint32(  0, arr[0], false );
        keyView.setUint32(  4, arr[1], false );
        keyView.setUint32(  8, arr[2], false );
        keyView.setUint32( 12, arr[3], false );
      
        let nonce = new Uint8Array(8), nonceView = new DataView(nonce.buffer);
        nonceView.setUint32(  0, arr[4], false );
        nonceView.setUint32(  4, arr[5], false );
      
        let iv = new Uint8Array(16);
        iv.set( nonce, 0 );

        return { key, iv };
    }

    /**
     * @param   {Array<string>}   ids
     * @returns {Promise<Array<object>>}
     */
    fetchMeta(ids)
    {
        if (! ids.length) {
            return Promise.all([]);
        }

        let data = ids.map(id => {
            return {
                a: "g",
                g: 1,
                ssl: 1,
                p: id
            };
        })

        return axios.post('https://g.api.mega.co.nz/cs', data, {
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            }
        }).then(response => {
            return response.data;
        });
    }
}

export default MegaBuilder;