import MegaCryptoDecryptor from "./decryptor/mega_crypto_decryptor";
import MegaCryptoJSDecryptor from "./decryptor/mega_cryptojs_decryptor";
import MegaAttrDecryptor from "./decryptor/mega_attr_decryptor";
import { MegaFileDecryptor } from "..";

class MegaDecryptStrategy
{
    /**
     * @param {MegaFileDecryptor} fileDecryptor 
     * @param {MegaAttrDecryptor|null} attrDecryptor 
     */
    constructor(fileDecryptor, attrDecryptor)
    {
        this.fileDecryptor = fileDecryptor;
        this.attrDecryptor = attrDecryptor;
    }

    /**
     * @param  {Uint8Array}  key
     * @param  {Uint8Array}  iv
     * @return {Promise<MegaDecryptStrategy>}
     */
    static create(key, iv)
    {
        let fileDecryptor = this._createFileDecryptor(key, iv);
        let attrDecryptor = MegaAttrDecryptor.create(key);

        return Promise.all([
            fileDecryptor,
            attrDecryptor
        ]).then(decryptors => {
            return new MegaDecryptStrategy(decryptors[0], decryptors[1]);
        });
    }

    /**
     * @private
     * @param  {Uint8Array}  key
     * @param  {Uint8Array}  iv
     * @return {Promise<MegaFileDecryptor>}
     */
    static _createFileDecryptor(key, iv)
    {
        if (typeof crypto !== 'undefined' && crypto.subtle) {
            return MegaCryptoDecryptor.create(key, iv).catch(() => {
                return MegaCryptoJSDecryptor.create(key, iv);
            })
        } else {
            return MegaCryptoJSDecryptor.create(key, iv);
        }
    }

    /**
     * @param  {ArrayBuffer}  str
     * @return {Promise<ArrayBuffer>}
     */
    decryptContent(str)
    {
        return this.fileDecryptor.decrypt(str);
    }

    /**
     * @param  {string}  str
     * @return {Promise<object>}
     */
    decryptAttributes(str)
    {
        if (this.attrDecryptor) {
            return this.attrDecryptor.decrypt(str);
        } else {
            return Promise.resolve({
                n: "Unknown"
            });
        }
    }
}

export default MegaDecryptStrategy;