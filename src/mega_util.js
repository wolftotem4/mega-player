class MegaUtil
{
    /**
     * @param {string} str
     */
    static megaToBase64(str)
    {
        return str.replace(/-/g, '+').replace(/_/g, '/').replace(/,/g, '');
    }

    /**
     * @param  {string}  url
     * @return {{ id: string, key: string }|null}
     */
    static parseMegaUrl(url)
    {
        let match = url.match(/^https:\/\/(mega\.co\.nz|mega\.nz)\/#\!([a-zA-Z0-9]+)\!([a-zA-Z0-9_,\-]+)/);
        if (match) {
            return {
                id  : match[2],
                key : match[3]
            };
        } else {
            return null;
        }
    }

    /**
     * @param  {string}  uri
     * @return {{ id: string, key: string }|null}
     */
    static parseMegaSchemeUri(uri)
    {
        let match = uri.match(/^mega:\/\/([a-zA-Z0-9]+)\!([a-zA-Z0-9_,\-]+)/i);
        if (match) {
            return {
                id  : match[1],
                key : match[2]
            };
        }
        return null;
    }

    /**
     * @param {{ id: string, key: string }} base 
     */
    static schemeToRealUrl(base)
    {
        return 'https://mega.nz/#!' + base.id + '!' + base.key;
    }
}

export default MegaUtil