import MegaBuilder from "./mega_builder";
import Mega from "./mega";
import MegaShakaRequestFilter from "./mega_shaka_request_filter";
import MegaShakaRequestManager from "./mega_shaka_request_manager";
import MegaUtil from "./mega_util";

class MegaManager
{
    constructor()
    {
        this.builder = new MegaBuilder;

        /**
         * @type {[mega_id: string]: Mega|Promise<Mega>}
         */
        this.resolves = {};
        this.requestFilter = new MegaShakaRequestFilter(this);
        this.schemePlugin = new MegaShakaRequestManager(this);
    }

    /**
     * @param  {string}  url
     * @return {Mega|null}
     */
    getExistant(url)
    {
        let resolves = this.resolves;
        for (var key in resolves) {
            var mega = resolves[key];
            if (mega instanceof Mega && mega.getUrl() === url) {
                return mega;
            }
        }
        return null;
    }

    /**
     * @param  {string}  url
     * @return {Promise<Mega>}
     */
    resolve(url)
    {
        return this.resolveMultiple([url]).then(megas => {
            if (megas.length) {
                return megas[0]
            } else {
                return Promise.reject(new Error('Invalid URL: ' + url));
            }
        });
    }

    /**
     * @param  {Array<string>}  urls
     * @return {Promise<Array<Mega>>}
     */
    resolveMultiple(urls)
    {
        return this.preloads(urls);
    }

    /**
     * @param  {Array<string>}  urls
     * @return {Promise<Array<Mega>>}
     */
    preloads(urls)
    {
        let bases = this.parseUrls(urls);
        let { resolves, rejects, prepares, promises } = this._prepareBases(bases);

        this.builder.create(prepares).then(objs => objs.map(mega => {
            this.resolves[mega.id] = mega;
            resolves[mega.id](mega);
        })).catch(e => {
            prepares.forEach(base => {
                rejects[base.id](e);
                if (base.id in this.resolves && ! (this.resolves[base.id] instanceof Mega)) {
                    delete this.resolves[base.id];
                }
            });
        })

        return Promise.all(promises);
    }

    /**
     * @param  {Array<string>}  urls
     * @return {Array<{ id: string, key: string }>}
     */
    parseUrls(urls)
    {
        return urls.map(MegaUtil.parseMegaUrl).filter(base => base !== null);
    }

    getRequestFilter()
    {
        return this.requestFilter.filter.bind(this.requestFilter);
    }

    getSchemePlugin()
    {
        return this.schemePlugin.shakaPlugin.bind(this.schemePlugin);
    }

    /**
     * @private
     * @param  {Array<{ id: string, key: string }>}  bases
     * @return {{ resolves: {[mega_id: string]: any }, rejects: {[mega_id: string]: any}, prepares: Array<{id: string, key: string}>, promises: Array<Mega|Promise<Mega>> }}
     */
    _prepareBases(bases)
    {
        let resolves = {};
        let rejects  = {}
        let prepares = [];
        let promises = bases.map(base => {
            let base_id = base.id;
            if (base_id in this.resolves) {
                return this.resolves[base_id];
            } else {
                return this.resolves[base_id] = new Promise((resolve, reject) => {
                    resolves[base_id] = resolve;
                    rejects[base_id]   = reject;
                    prepares.push(base);
                });
            }
            return null;
        }).filter(promise => promise !== null)
        return { resolves, rejects, prepares, promises };
    }
}

export default MegaManager