import MegaManager from "./mega_manager";

class MegaPlayer
{
    constructor()
    {
        this.manager = new MegaManager();
    }

    /**
     * @param {shaka.net.NetworkingEngine} engine
     */
    registerNetworkingEngine(engine)
    {
        engine.registerRequestFilter(this.manager.getRequestFilter());
        shaka.net.NetworkingEngine.registerScheme('mega', this.manager.getSchemePlugin(), shaka.net.NetworkingEngine.PluginPriority.PREFERRED);
    }
}

const player = new MegaPlayer

export default player
export const manager = player.manager