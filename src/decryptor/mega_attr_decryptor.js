import * as CryptoJS from 'crypto-js';
import MegaUtil from '../mega_util';

class MegaAttrDecryptor
{
    /**
     * @param  {CryptoJS.LibWordArray}
     */
    constructor(key)
    {
        this.key = key;
    }

    /**
     * @param {Uint8Array} key 
     */
    static create(key)
    {
        return Promise.resolve(new MegaAttrDecryptor(
            CryptoJS.lib.WordArray.create(key)
        ));
    }

    /**
     * @param  {string}  str
     * @param  {MegaAttrDecryptor}  decryptor
     * @return {Promise<object>}
     */
    decrypt(str)
    {
        let cipherParams = CryptoJS.lib.CipherParams.create({
            ciphertext: CryptoJS.enc.Base64.parse(MegaUtil.megaToBase64(str))
        });
        let options = {
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.NoPadding,
            iv: CryptoJS.lib.WordArray.create([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
        };

        try {
            var decrypted = CryptoJS.AES.decrypt(cipherParams, this.key, options).toString(CryptoJS.enc.Utf8);
            if (decrypted.substr(0, 6) == 'MEGA{"') {
                var json = decrypted.substr(4).replace(/\x00/g, '');
                return Promise.resolve(JSON.parse(json));
            }
        } catch(e) {
            return Promise.reject(e);
        }

        return Promise.reject(new Error('Failed to decrypt the attributes.'));
    }
}

export default MegaAttrDecryptor;