class MegaCryptoDecryptor
{
    /**
     * @param {SubtleCrypto} decryptor
     * @param {CryptoKey} key
     * @param {Uint8Array} iv
     */
    constructor(decryptor, key, iv)
    {
        this.decryptor = decryptor;
        this.key = key;
        this.iv = iv;
    }

    /**
     * 
     * @param  {Uint8Array}  key
     * @param  {Uint8Array}  iv
     * @return {Promise<MegaCryptoDecryptor>}
     */
    static create(key, iv)
    {
        return crypto.subtle.importKey(
            'raw',
            key,
            { name: "AES-CTR" },
            false,
            ["decrypt"]
        ).then(key => {
            return new MegaCryptoDecryptor(crypto.subtle, key, iv);
        });
    }

    /**
     * @param  {number}  pos
     * @return {MegaCryptoDecryptor}
     */
    start(pos)
    {
        let new_iv;

        if (pos) {
            new_iv = new Uint8Array(16);
            let dataView = new DataView(new_iv.buffer);
            new_iv.set( this.iv, 0 );
            dataView.setUint32(12, pos / 16, false);

        } else {
            new_iv = this.iv;
        }

        return new MegaCryptoDecryptor(this.decryptor, this.key, new_iv);
    }

    /**
     * 
     * @param  {ArrayBuffer}  data
     * @return {Promise<ArrayBuffer>}
     */
    decrypt(data)
    {
        return this.decryptor.decrypt({
            name: 'AES-CTR',
            counter: this.iv,
            length: 128
        }, this.key,
            data
        );
    }
}

export default MegaCryptoDecryptor;