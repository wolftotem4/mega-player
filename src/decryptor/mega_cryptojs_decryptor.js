import * as CryptoJS from 'crypto-js';

class MegaCryptoJSDecryptor
{
    /**
     * @param {CryptoJS.LibWordArray} key
     * @param {Uint8Array} iv
     */
    constructor(key, iv)
    {
        this.key = key;
        this.iv = iv;
    }

    /**
     * 
     * @param  {Uint8Array}  key
     * @param  {Uint8Array}  iv
     * @return {Promise<MegaCryptoJSDecryptor>}
     */
    static create(key, iv)
    {
        return Promise.resolve(new MegaCryptoJSDecryptor(
            CryptoJS.lib.WordArray.create(key),
            iv
        ));
    }

    /**
     * @param  {number}  pos
     * @return {MegaCryptoJSDecryptor}
     */
    start(pos)
    {
        let new_iv = this.iv;

        if (pos)
        {
            new_iv = new Uint8Array(16);
            let dataView = new DataView(new_iv.buffer);
            new_iv.set( this.iv, 0 );
            dataView.setUint32(12, pos / 16, false);
        }

        return new MegaCryptoJSDecryptor(this.key, new_iv);
    }

    /**
     * @return {CryptoJS.Decryptor}
     */
    getDecryptor()
    {
        return CryptoJS.algo.AES.createDecryptor(
            this.key,
            {
                mode: CryptoJS.mode.CTR,
                iv: CryptoJS.lib.WordArray.create(this.iv),
                padding: CryptoJS.pad.NoPadding
            }
        );
    }

    /**
     * @param  {ArrayBuffer}  data
     * @return {Promise<ArrayBuffer>}
     */
    decrypt(data)
    {
        let buffer = CryptoJS.lib.WordArray.create(data);
        return Promise.resolve(this.stringify(this.getDecryptor().finalize(buffer)).buffer);
    }

    /**
     * @param  {CryptoJS.LibWordArray}  wordArray
     * @return {Uint8Array}
     */
    stringify(wordArray) {
        // Shortcuts
        var words = wordArray.words;
        var sigBytes = wordArray.sigBytes;

        // Convert
        var u8 = new Uint8Array(sigBytes);
        for (var i = 0; i < sigBytes; i++) {
            var _byte = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
            u8[i]=_byte;
        }

        return u8;
    }
}

export default MegaCryptoJSDecryptor;