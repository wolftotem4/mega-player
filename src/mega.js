import { AxiosRequestConfig, AxiosResponse } from 'axios';
import mime from 'mime/lite';
import { MegaFileDecryptor } from '..';
import MegaRequest from './mega_request';

class Mega
{
    /**
     * @param {string} id
     * @param {MegaFileDecryptor} decryptor
     * @param {string} url
     * @param {string} filename
     * @param {number} filesize
     */
    constructor(id, decryptor, url, filename, filesize)
    {
        this.id = id;
        this.decryptor = decryptor;
        this.filename = filename;
        this.filesize = filesize;

        this.setUrl(url);
    }
    
    /**
     * @return {string}
     */
    getUrl()
    {
        return this.origin + this.pathname;
    }

    /**
     * 
     * @param  {string}  url 
     * @return {ThisType<Mega>}
     */
    setUrl(url)
    {
        var scheme = document.createElement('a');
        scheme.setAttribute('href', url);
        this.origin = scheme['origin'] || scheme['protocol'] + "//" + scheme['hostname'];
        this.pathname = scheme['pathname'].replace(/(^\/?)/,"/");
        return this
    }

    /**
     * @return string|null
     */
    guessType()
    {
        return mime.getType(this.filename);
    }

    /**
     * 
     * @param  {AxiosRequestConfig}  config
     * @return {Promise<AxiosResponse>}
     */
    request(config = {})
    {
        return new MegaRequest(this).request(config);
    }

    /**
     * @param  {AxiosRequestConfig}  config
     * @return {Promise<ArrayBuffer>}
     */
    download(config = {})
    {
        return this.request(config).then(response => response.data);
    }
}

export default Mega;