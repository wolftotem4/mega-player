
import MegaManager from './mega_manager'
import MegaRequest from './mega_request';
import MegaUtil from './mega_util';

class MegaShakaRequestFilter
{
    /**
     * @param {MegaManager} manager
     */
    constructor(manager)
    {
        this.manager = manager;
    }

    /**
     * @param  {shaka.net.NetworkingEngine.RequestType}  type
     * @param  {shakaExtern.Request}  request
     */
    filter(type, request)
    {
        request.uris = request.uris.map(uri => {
            let base = MegaUtil.parseMegaUrl(uri)
            return (base !== null) ? 'mega://' + base.id + '!' + base.key : uri;
        });
    }
}

export default MegaShakaRequestFilter;