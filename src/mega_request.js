import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import Mega from "./mega";
import MegaCryptoJSDecryptor from "./decryptor/mega_cryptojs_decryptor";

class MegaRequest
{
    /**
     * @param {Mega} mega
     */
    constructor(mega)
    {
        this.mega = mega;
    }

    /**
     * @param  {AxiosRequestConfig}  config
     * @return {Promise<AxiosResponse>}
     */
    request(config = {})
    {
        let range = this._extractRange(config.headers || {});

        config = Object.assign({}, config, {
            url: this.mega.getUrl(),
            responseType: 'arraybuffer'
        })

        if (range && (range.start > 0 || range.end < this._getEofPosition())) {
            let { skipBytes, startPos, endPos, length } = MegaRequest.position(
                range.start,
                range.end,
                this.mega.filesize
            );

            config.url += '/' + startPos + '-' + endPos;

            return axios(config).then(response => {
                let decryptor = this.mega.decryptor.start(startPos);
                return decryptor.decrypt(response.data).then(decrypted => {
                    response.data = decrypted.slice(skipBytes, skipBytes + length)
                    return response
                });
            })
        } else {
            return axios(config).then(response => {
                return this.mega.decryptor.decrypt(response.data).then(decrypted => {
                    response.data = decrypted;
                    return response
                })
            })
        }
    }

    /**
     * @param  {number}  start
     * @param  {number}  end
     * @param  {number?} filesize
     * @return {{ skipBytes: number, startPos: number, endPos: number, length: number }}
     */
    static position(start, end, filesize)
    {
        var skipBytes = start % 16,
            startPos  = start - skipBytes,
            extBytes  = 16 - (end % 16) - 1,
            endPos    = end + extBytes,
            length    = end - start + 1;

        if (filesize) {
            endPos = Math.min(filesize - 1, endPos)
        }

        return {
            skipBytes,
            startPos,
            endPos,
            length
        }
    }

    /**
     * @private
     * @param {{[key:string]: any}} headers
     */
    _extractRange(headers)
    {
        for (var key in headers) {
            if (key.toLowerCase() == 'range') {
                var match = headers[key].match(/bytes\=(\d+)\-(\d+)?/i);
                if (match) {
                    delete headers[key];
                    return {
                        start: parseInt(match[1]),
                        end: (match[2] === undefined) ? this._getEofPosition() : parseInt(match[2])
                    }
                }
            }
        }
        return null
    }

    /**
     * @private
     * @return {number}
     */
    _getEofPosition()
    {
        return this.mega.filesize - 1;
    }
}

export default MegaRequest