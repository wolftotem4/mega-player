import axios from 'axios'
import MegaManager from './mega_manager'
import MegaUtil from './mega_util';

class MegaShakaRequestManager
{
    /**
     * @param {MegaManager} manager 
     */
    constructor(manager)
    {
        this.manager = manager;
    }

    /**
     * @param  {string} uri
     * @param  {shakaExtern.Request} request
     * @param  {shaka.net.NetworkingEngine.RequestType} requestType
     * @return {Promise<shakaExtern.Response>}
     */
    shakaPlugin(uri, request, requestType)
    {
        let base = MegaUtil.parseMegaSchemeUri(uri);
        if (base === null) {
            return Promise.reject(new Error('Invalid Uri: ' + uri));
        }

        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        let promise = this.manager.resolve(MegaUtil.schemeToRealUrl(base)).then(mega => {
            return mega.request({
                method: request.method || 'GET',
                timeout: request.retryParameters.timeout,
                withCredentials: request.allowCrossSiteCredentials,
                headers: request.headers,
                cancelToken: source.token
            }).then(response => {
                let guessType = mega.guessType();
                if (guessType) {
                    response.headers['content-type'] = mega.guessType();
                }

                return this._makeResponse(
                    response.headers,
                    response.data,
                    response.status,
                    uri,
                    request.url,
                    requestType
                );
            })
        })

        return new shaka.util.AbortableOperation(
            promise,
            () => {
                source.cancel();
                return Promise.resolve();
            }
        );
    }

    /**
     * @private
     * @param {{[key: string]: string}} headers 
     * @param {ArrayBuffer?} data 
     * @param {number} status 
     * @param {string} uri 
     * @param {string} responseURL 
     * @param {shaka.net.NetworkingEngine.RequestType} requestType 
     */
    _makeResponse(headers, data, status, uri, responseURL, requestType)
    {
        if (status >= 200 && status <= 299 && status != 202) {
            // Most 2xx HTTP codes are success cases.
            if (responseURL) {
                uri = responseURL;
            }
            /** @type {shakaExtern.Response} */
            let response = {
                uri: uri,
                data: data,
                headers: headers,
                fromCache: !!headers['x-shaka-from-cache']
            };
            return response;
        } else {
            let responseText = null;
            try {
                responseText = shaka.util.StringUtils.fromBytesAutoDetect(data);
            } catch (exception) {}
            shaka.log.debug('HTTP error text:', responseText);

            let severity = status == 401 || status == 403 ?
                shaka.util.Error.Severity.CRITICAL :
                shaka.util.Error.Severity.RECOVERABLE;
            throw new shaka.util.Error(
                severity,
                shaka.util.Error.Category.NETWORK,
                shaka.util.Error.Code.BAD_HTTP_STATUS,
                uri,
                status,
                responseText,
                headers,
                requestType);
        }
    }
}

export default MegaShakaRequestManager;